package com.success.android.bakingapp;

import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.success.android.bakingapp.testhelpers.CustomIdlingResource;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    private IdlingResource idlingResource;

    @Rule
    public IntentsTestRule<MainActivity> activityTestRule =
            new IntentsTestRule<>(MainActivity.class, true, false);

    @Before
    public void initializeIdlingResource() {
        CustomIdlingResource.setupIdlingResource();
        idlingResource = CustomIdlingResource.getInstance();
        IdlingRegistry.getInstance().register(idlingResource);
    }

    @Test
    public void RecipeCardClick_IsStepsDisplayedTest() {
        activityTestRule.launchActivity(null);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.rv_list))
                .perform(scrollToPosition(0))
                .perform(actionOnItemAtPosition(0, click()));
        onView(withText(R.string.step_list_ingredients_title)).check(matches(isDisplayed()));
    }

    @Test
    public void BackButtonTest() {
        activityTestRule.launchActivity(null);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.rv_list))
                .perform(scrollToPosition(0))
                .perform(actionOnItemAtPosition(0, click()));
        pressBack();
        onView(withText("Yellow Cake")).check(matches(isDisplayed()));
        onView(withId(R.id.rv_list))
                .perform(scrollToPosition(0))
                .perform(actionOnItemAtPosition(0, click()));
        onView(withText(R.string.step_list_ingredients_title)).perform(click());
        pressBack();
        pressBack();
        onView(withId(R.id.rv_list))
                .perform(scrollToPosition(2));
        onView(withText("Yellow Cake")).check(matches(isDisplayed()));
    }

    @After
    public void unregisterIdlingResources() {
        IdlingRegistry.getInstance().unregister(idlingResource);
    }
}