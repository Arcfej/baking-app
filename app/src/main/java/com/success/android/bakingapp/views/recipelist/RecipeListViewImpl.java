package com.success.android.bakingapp.views.recipelist;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.success.android.bakingapp.R;

public class RecipeListViewImpl implements RecipeListView {

    private final View rootView;

    public RecipeListViewImpl(LayoutInflater inflater, ViewGroup container,
                              RecyclerView.Adapter presenter, boolean isTablet, boolean isPortrait) {
        rootView = inflater.inflate(R.layout.fragment_list, container, false);

        RecyclerView.LayoutManager layoutManager;
        if (isTablet && !isPortrait) {
            layoutManager = new GridLayoutManager(rootView.getContext(), 4);
        } else if (!isTablet && isPortrait) {
            layoutManager = new LinearLayoutManager(rootView.getContext());
        } else {
            layoutManager = new GridLayoutManager(rootView.getContext(), 2);
        }
        ((RecyclerView) rootView).setLayoutManager(layoutManager);

        ((RecyclerView) rootView).setAdapter(presenter);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }
}
