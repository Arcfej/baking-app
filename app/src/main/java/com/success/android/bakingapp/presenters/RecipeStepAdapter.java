package com.success.android.bakingapp.presenters;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.success.android.bakingapp.R;
import com.success.android.bakingapp.pojos.Recipe;
import com.success.android.bakingapp.views.BaseView;
import com.success.android.bakingapp.views.steplist.StepHolder;
import com.success.android.bakingapp.views.steplist.StepListView;

import org.greenrobot.eventbus.Subscribe;

public class RecipeStepAdapter extends RecyclerView.Adapter<StepHolder> implements BasePresenter {

    private static final String KEY_STATE_SELECTED_ITEM = "selected_item";

    private static final int VIEW_TYPE_INGREDIENTS = 23;
    private static final int VIEW_TYPE_STEP = 28;

    private StepListView listView;

    private Recipe recipe;
    private Resources resources;

    private int activatedItem;

    public RecipeStepAdapter(Recipe recipe, Resources resources, Bundle presenterState) {
        this.recipe = recipe;
        this.resources = resources;
        if (presenterState != null && presenterState.containsKey(KEY_STATE_SELECTED_ITEM)) {
            activatedItem = presenterState.getInt(KEY_STATE_SELECTED_ITEM);
        }
    }

    @Override
    public void attachView(BaseView view) {
        listView = (StepListView) view;
    }

    @Override
    public Bundle getState() {
        Bundle state = new Bundle();
        state.putInt(KEY_STATE_SELECTED_ITEM, activatedItem);
        return state;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return VIEW_TYPE_INGREDIENTS;
        } else {
            return VIEW_TYPE_STEP;
        }
    }

    @NonNull
    @Override
    public StepHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return StepHolder.constructWithInflating(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull StepHolder holder, int position) {
        holder.setOnStepClickListener(recipe);
        if (getItemViewType(position) == VIEW_TYPE_INGREDIENTS) {
            holder.setText(resources.getString(R.string.step_list_ingredients_title));
            if (resources.getBoolean(R.bool.isTablet) && position == activatedItem) {
                holder.itemView.performClick();
            }
        } else {
            holder.setText(recipe.getSteps().get(position - 1).getShortDescription());
        }
        holder.itemView.setActivated(position == activatedItem);
    }

    @Override
    public int getItemCount() {
        return recipe.getSteps().size() + 1;
    }

    @Subscribe
    public void onStepClicked(StepHolder.StepClickEvent event) {
        activatedItem = event.clickedPosition;
        notifyDataSetChanged();
    }
}
