package com.success.android.bakingapp.presenters;

public interface StepDetailsPresenter extends BasePresenter {

    void preparePlayer();

    void startPlayingVideo();

    void resetPlayer();

    void releasePlayer();
}
