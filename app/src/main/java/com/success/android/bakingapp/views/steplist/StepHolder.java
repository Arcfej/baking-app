package com.success.android.bakingapp.views.steplist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.success.android.bakingapp.R;
import com.success.android.bakingapp.pojos.Recipe;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StepHolder extends RecyclerView.ViewHolder implements StepListItem {

    @BindView(R.id.tv_recipe_step_list_item)
    TextView stepTitleView;

    protected StepHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public static StepHolder constructWithInflating(ViewGroup parent) {
        View itemView =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_recipe_step, parent, false);
        return new StepHolder(itemView);
    }

    @Override
    public void setText(String text) {
        stepTitleView.setText(text);
    }

    @Override
    public void setOnStepClickListener(final Recipe recipe) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new StepClickEvent(recipe, getAdapterPosition()));
            }
        });
    }

    public static class StepClickEvent {

        public final Recipe recipe;
        public final int clickedPosition;

        public StepClickEvent(Recipe recipe, int clickedPosition) {
            this.recipe = recipe;
            this.clickedPosition = clickedPosition;
        }
    }
}