package com.success.android.bakingapp.model.apirecipe;

public final class ApiRecipeContract {

    public static final String URL_RECIPE_LIST = "http://go.udacity.com/android-baking-app-json";

    public static final String RECIPE_ID = "id";
    public static final String RECIPE_NAME = "name";
    public static final String RECIPE_INGREDIENTS_LIST = "ingredients";
    public static final String RECIPE_STEPS_LIST = "steps";
    public static final String RECIPE_SERVINGS = "servings";
    public static final String RECIPE_IMAGE = "image";

    public static final String INGREDIENT_QUANTITY = "quantity";
    public static final String INGREDIENT_MEASURE = "measure";
    public static final String INGREDIENT = "ingredient";

    public static final String STEP_ID = "id";
    public static final String STEP_SHORT_DESCRIPTION = "shortDescription";
    public static final String STEP_DESCRIPTION = "description";
    public static final String STEP_VIDEO_URL = "videoURL";
    public static final String STEP_THUMBNAIL_URL = "thumbnailURL";

    private ApiRecipeContract() {
        throw new AssertionError("You shouldn't have any instances of this class");
    }
}
