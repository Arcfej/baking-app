package com.success.android.bakingapp.model;

import android.util.Log;

import com.success.android.bakingapp.model.apirecipe.ApiRecipeContract;
import com.success.android.bakingapp.pojos.Recipe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public final class JsonHelper {

    private static final String TAG = JsonHelper.class.getSimpleName();

    private JsonHelper() {
        throw new AssertionError("You shouldn't have any instances of this class");
    }

    public static List<Recipe> parseRecipeListFromJsonArray(JSONArray jsonArray) {
        if (jsonArray == null || jsonArray.length() == 0) {
            return new ArrayList<>(0);
        }
        List<Recipe> recipeList = new ArrayList<>(jsonArray.length());
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jRecipe = jsonArray.getJSONObject(i);
                int id = jRecipe.getInt(ApiRecipeContract.RECIPE_ID);
                String name = jRecipe.optString(
                        ApiRecipeContract.RECIPE_NAME, "Recipe Number " + i);
                JSONArray jIngredients =
                        jRecipe.getJSONArray(ApiRecipeContract.RECIPE_INGREDIENTS_LIST);
                List<Recipe.Ingredient> ingredients = parseIngredientListFromJsonArray(jIngredients);
                JSONArray jSteps = jRecipe.getJSONArray(ApiRecipeContract.RECIPE_STEPS_LIST);
                List<Recipe.Step> steps = parseStepListFromJsonArray(jSteps);
                int servings = jRecipe.optInt(ApiRecipeContract.RECIPE_SERVINGS, 1);
                String imagePath = jRecipe.optString(ApiRecipeContract.RECIPE_IMAGE, null);
                Recipe recipe = new Recipe(id, name, ingredients, steps, servings, imagePath);
                recipeList.add(recipe);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Problem during parsing the JSONArray to a list of Recipes:");
            e.printStackTrace();
            return new ArrayList<>(0);
        }
        return recipeList;
    }

    public static List<Recipe.Ingredient> parseIngredientListFromJsonArray(JSONArray jsonArray) {
        if (jsonArray == null || jsonArray.length() == 0) {
            return new ArrayList<>(0);
        }
        List<Recipe.Ingredient> ingredientList = new ArrayList<>(jsonArray.length());
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jIngredient = jsonArray.getJSONObject(i);
                int quantity = jIngredient.getInt(ApiRecipeContract.INGREDIENT_QUANTITY);
                String measure = jIngredient.getString(ApiRecipeContract.INGREDIENT_MEASURE);
                String ingredientName = jIngredient.getString(ApiRecipeContract.INGREDIENT);
                Recipe.Ingredient ingredient =
                        new Recipe.Ingredient(quantity, measure, ingredientName);
                ingredientList.add(ingredient);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Problem during parsing the JSONArray to a list of Ingredients");
            e.printStackTrace();
            return new ArrayList<>(0);
        }
        return ingredientList;
    }

    public static List<Recipe.Step> parseStepListFromJsonArray(JSONArray jsonArray) {
        if (jsonArray == null || jsonArray.length() == 0) {
            return new ArrayList<>(0);
        }
        List<Recipe.Step> stepList = new ArrayList<>(jsonArray.length());
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jStep = jsonArray.getJSONObject(i);
                int id = jStep.getInt(ApiRecipeContract.STEP_ID);
                String shortDescription = jStep.getString(ApiRecipeContract.STEP_SHORT_DESCRIPTION);
                String description = jStep.getString(ApiRecipeContract.STEP_DESCRIPTION);
                String videoUrl = jStep.optString(ApiRecipeContract.STEP_VIDEO_URL, null);
                String thumbnailUrl = jStep.optString(ApiRecipeContract.STEP_THUMBNAIL_URL, null);
                Recipe.Step step =
                        new Recipe.Step(id, shortDescription, description, videoUrl, thumbnailUrl);
                stepList.add(step);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Problem during parsing the JSONArray to a list of Steps");
            e.printStackTrace();
            return new ArrayList<>(0);
        }
        return stepList;
    }
}
