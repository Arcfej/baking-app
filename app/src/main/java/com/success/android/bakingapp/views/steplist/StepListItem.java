package com.success.android.bakingapp.views.steplist;

import com.success.android.bakingapp.pojos.Recipe;

public interface StepListItem {

    void setText(String text);

    void setOnStepClickListener(Recipe recipe);
}