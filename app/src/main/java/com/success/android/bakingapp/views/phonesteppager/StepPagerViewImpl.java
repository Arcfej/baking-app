package com.success.android.bakingapp.views.phonesteppager;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.success.android.bakingapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StepPagerViewImpl implements StepPagerView {

    private final View rootView;
    @BindView(R.id.vp_recipe_step_pager)
    ViewPager viewPager;
    @BindView(R.id.pts_recipe_step_pager_tab)
    TabLayout pagerTab;

    public StepPagerViewImpl(LayoutInflater inflater, ViewGroup container,
                             PagerAdapter presenter, int position) {
        rootView = inflater.inflate(R.layout.fragment_phone_step_pager, container, false);
        ButterKnife.bind(this, rootView);

        viewPager.setAdapter(presenter);
        pagerTab.setupWithViewPager(viewPager);

        pagerTab.getTabAt(position).select();
        viewPager.setCurrentItem(position);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    public static class PagerPageChangedEvent {

        public final int visiblePageIndex;

        public PagerPageChangedEvent(int visiblePageIndex) {
            this.visiblePageIndex = visiblePageIndex;
        }
    }
}