package com.success.android.bakingapp.views.stepdetails;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;
import com.success.android.bakingapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StepDetailsViewImpl implements StepDetailsView {

    private final View rootView;
    @BindView(R.id.pv_video_player)
    PlayerView playerView;
    @BindView(R.id.tv_recipe_step_instructions)
    TextView stepInstructions;

    public StepDetailsViewImpl(LayoutInflater inflater, ViewGroup container) {
        rootView = inflater.inflate(R.layout.fragment_step_details, container, false);
        ButterKnife.bind(this, rootView);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    @Override
    public void setVideoPlayer(ExoPlayer player) {
        playerView.setVisibility(View.VISIBLE);
        playerView.setPlayer(player);
    }

    @Override
    public void setInstructions(String instructions) {
        stepInstructions.setText(instructions);
    }
}