package com.success.android.bakingapp.views.recipelist;

import com.success.android.bakingapp.pojos.Recipe;

public interface RecipeListItem {

    void setRecipeName(String name);

    void setRecipeServings(int count);

    void setRecipeBackground(String uri);

    void setOnRecipeClickListener(Recipe recipe);
}
