package com.success.android.bakingapp.presenters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.success.android.bakingapp.model.RecipeDataManager;
import com.success.android.bakingapp.model.RecipeDataManagerImpl;
import com.success.android.bakingapp.pojos.Recipe;
import com.success.android.bakingapp.testhelpers.CustomIdlingResource;
import com.success.android.bakingapp.views.BaseView;
import com.success.android.bakingapp.views.recipelist.RecipeHolder;
import com.success.android.bakingapp.views.recipelist.RecipeListView;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class RecipeCardAdapter extends RecyclerView.Adapter<RecipeHolder> implements BasePresenter {

    @VisibleForTesting
    @Nullable
    private CustomIdlingResource idlingResource;

    private final RecipeDataManager dataManager;
    private RecipeListView listView;

    private List<Recipe> recipeList = new ArrayList<>(0);

    public RecipeCardAdapter(RecipeDataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(BaseView view) {
        listView = (RecipeListView) view;
        dataManager.requestRecipeList();
    }

    @Override
    public Bundle getState() {
        return null;
    }

    @NonNull
    @Override
    public RecipeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return RecipeHolder.constructWithInflating(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeHolder holder, int position) {
        Recipe recipe = recipeList.get(position);
        if (recipe != null) {
            holder.setRecipeName(recipe.getName());
            holder.setRecipeServings(recipe.getServings());
            holder.setRecipeBackground(recipe.getImagePath());
            holder.setOnRecipeClickListener(recipe);
        }
    }

    @Override
    public int getItemCount() {
        return recipeList.size();
    }

    @Subscribe
    public void onRecipeListReady(RecipeDataManagerImpl.RecipeListReadyEvent event) {
        recipeList = event.recipeList;
        notifyDataSetChanged();
        if (CustomIdlingResource.getInstance() != null) {
            CustomIdlingResource.getInstance().setIdleState(true);
        }
    }
}