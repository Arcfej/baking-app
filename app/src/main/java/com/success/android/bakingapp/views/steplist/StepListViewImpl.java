package com.success.android.bakingapp.views.steplist;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.success.android.bakingapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StepListViewImpl implements StepListView {

    private final View rootView;
    @BindView(R.id.rv_list)
    public RecyclerView list;

    public StepListViewImpl(LayoutInflater inflater, ViewGroup container,
                              RecyclerView.Adapter presenter) {
        rootView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, rootView);

        list.setAdapter(presenter);
        list.setLayoutManager(new LinearLayoutManager(container.getContext()));
        list.addItemDecoration(new DividerItemDecoration(list.getContext(), LinearLayoutManager.VERTICAL));
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    @Override
    public void clickListItem(int position) {
        list.findViewHolderForAdapterPosition(position).itemView.performClick();
    }
}
