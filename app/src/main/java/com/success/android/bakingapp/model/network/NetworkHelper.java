package com.success.android.bakingapp.model.network;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.interfaces.JSONArrayRequestListener;

import java.util.Map;

/**
 * Created by Noki 2 on 2018. 03. 19..
 */

public final class NetworkHelper {

    private NetworkHelper() {
        throw new AssertionError("You shouldn't have any instances of this class");
    }

    public static void startDownloadJSONArray(@NonNull Uri url,
                                              @Nullable Map<String, String> query,
                                              @Nullable String tag,
                                              @NonNull JSONArrayRequestListener listener) {
        ANRequest.GetRequestBuilder builder = AndroidNetworking.get(url.toString());
        if (query != null) {
            builder.addQueryParameter(query);
        }
        if (!TextUtils.isEmpty(tag)) {
            builder.setTag(tag);
        }
        builder.setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(listener);
    }

    public static void prefetchDownload(@NonNull Uri url,
                                          @Nullable Map<String, String> query,
                                          @Nullable String tag) {
        ANRequest.GetRequestBuilder builder = AndroidNetworking.get(url.toString());
        if (query != null) {
            builder.addQueryParameter(query);
        }
        if (!TextUtils.isEmpty(tag)) {
            builder.setTag(tag);
        }
        builder.setPriority(Priority.MEDIUM)
                .build()
                .prefetch();
    }

    public static void cancelDownload(String tag) {
        AndroidNetworking.cancel(tag);
    }
}
