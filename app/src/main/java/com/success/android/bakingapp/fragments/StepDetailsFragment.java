package com.success.android.bakingapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.success.android.bakingapp.model.RecipeDataManager;
import com.success.android.bakingapp.model.RecipeDataManagerImpl;
import com.success.android.bakingapp.pojos.Recipe;
import com.success.android.bakingapp.presenters.StepDetailsPresenterImpl;
import com.success.android.bakingapp.views.stepdetails.StepDetailsView;
import com.success.android.bakingapp.views.stepdetails.StepDetailsViewImpl;

public class StepDetailsFragment extends android.support.v4.app.Fragment {

    public static final String KEY_ARGUMENTS_RECIPE = "recipe";
    public static final String KEY_ARGUMENTS_STEP_INDEX = "position";

    private static final String KEY_RECIPE = "recipe";
    private static final String KEY_STEP_INDEX = "position";
    private static final String STATE_PRESENTER = "presenter_state";

    private RecipeDataManager dataManager;

    // Need to save with state
    private Recipe recipe;
    private int stepIndex;
    private Bundle presenterState;
    //

    private StepDetailsPresenterImpl presenter;
    private StepDetailsView view;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataManager = RecipeDataManagerImpl.getInstance();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_RECIPE)) {
            if (savedInstanceState.containsKey(KEY_RECIPE)) {
                recipe = savedInstanceState.getParcelable(KEY_RECIPE);
            }
            if (savedInstanceState.containsKey(KEY_STEP_INDEX)) {
                stepIndex = savedInstanceState.getInt(KEY_STEP_INDEX);
            }
            if (savedInstanceState.containsKey(STATE_PRESENTER)) {
                presenterState = savedInstanceState.getBundle(STATE_PRESENTER);
            }
        } else if (getArguments() == null || !getArguments().containsKey(KEY_ARGUMENTS_RECIPE)) {
            throw new IllegalArgumentException("This fragment need a Recipe (in the arguments");
        } else {
            recipe = getArguments().getParcelable(KEY_ARGUMENTS_RECIPE);
            stepIndex = getArguments().getInt(KEY_ARGUMENTS_STEP_INDEX);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // getContext will never be null, because the fragment must never retain it's instance!!!
        presenter = new StepDetailsPresenterImpl(
                getContext(), dataManager, recipe, stepIndex, presenterState);
        view = new StepDetailsViewImpl(inflater, container);
        return view.getRootView();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.attachView(view);
        presenter.preparePlayer();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (this.getUserVisibleHint()) {
            presenter.startPlayingVideo();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.resetPlayer();
        presenterState = presenter.getState();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_RECIPE, recipe);
        outState.putInt(KEY_STEP_INDEX, stepIndex);
        outState.putBundle(STATE_PRESENTER, presenter.getState());
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.releasePlayer();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (presenter == null) return;
        if (isVisibleToUser) {
            presenter.preparePlayer();
            presenter.startPlayingVideo();
        } else {
            presenter.resetPlayer();
            presenter.releasePlayer();
        }
    }
}