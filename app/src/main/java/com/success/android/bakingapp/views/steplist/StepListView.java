package com.success.android.bakingapp.views.steplist;

import com.success.android.bakingapp.views.BaseView;

public interface StepListView extends BaseView {

    void clickListItem(int position);
}