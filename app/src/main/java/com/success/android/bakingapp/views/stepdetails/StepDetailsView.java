package com.success.android.bakingapp.views.stepdetails;

import com.google.android.exoplayer2.ExoPlayer;
import com.success.android.bakingapp.views.BaseView;

public interface StepDetailsView extends BaseView {

    void setVideoPlayer(ExoPlayer player);

    void setInstructions(String instructions);
}