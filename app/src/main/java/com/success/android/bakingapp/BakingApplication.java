package com.success.android.bakingapp;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;

import com.androidnetworking.AndroidNetworking;
import com.example.myapp.MyEventBusIndex;

import org.greenrobot.eventbus.EventBus;

public class BakingApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Default EventBus for the presenters and views
        EventBus.builder().addIndex(new MyEventBusIndex()).installDefaultEventBus();

        // Initialize Networking library
        AndroidNetworking.initialize(getApplicationContext());
    }

    public static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }
}