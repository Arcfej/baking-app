package com.success.android.bakingapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.success.android.bakingapp.fragments.RecipeListFragment;
import com.success.android.bakingapp.fragments.StepListFragment;
import com.success.android.bakingapp.fragments.TabletRecipeFragment;
import com.success.android.bakingapp.fragments.ViewPagerFragment;
import com.success.android.bakingapp.model.RecipeDataManager;
import com.success.android.bakingapp.model.RecipeDataManagerImpl;
import com.success.android.bakingapp.pojos.Recipe;
import com.success.android.bakingapp.testhelpers.CustomIdlingResource;
import com.success.android.bakingapp.views.recipelist.RecipeHolder;
import com.success.android.bakingapp.views.steplist.StepHolder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MainActivity extends AppCompatActivity {

    private static final String TAG_FRAGMENT_RECIPE_LIST = "recipes";
    private static final String TAG_FRAGMENT_STEP_LIST = "steps";
    private static final String TAG_FRAGMENT_PHONE_STEP_PAGER = "step_pager";
    private static final String TAG_FRAGMENT_TABLET_RECIPE = "tablet_recipe";

    public static final int REQUEST_CODE_OPEN_FROM_INGREDIENTS_WIDGET = 123;
    public static final String KEY_INTENT_EXTRA_RECIPE = "recipe";

    // Is the app running on a tablet?
    boolean isTablet;

    private RecipeDataManager dataManager;

    // Used both on phones and tablets
    private Fragment recipeList;

    // Used only on phones!
    private Fragment phoneStepPager;
    private Fragment stepList;

    // Used only on tablets!
    private Fragment tabletRecipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isTablet = getResources().getBoolean(R.bool.isTablet);

        dataManager = RecipeDataManagerImpl.getInstance();

//        // set the orientation for tablet and phone differently
//        if (isTablet) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
//        } else {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        }
        setContentView(R.layout.activity_main);

        initializeFragments();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initializeFragments() {
        recipeList = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_RECIPE_LIST);
        if (recipeList == null) {
            recipeList = new RecipeListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fl_main_fragment_holder, recipeList, TAG_FRAGMENT_RECIPE_LIST)
                    .commit();
        }

        if (isTablet) {
            tabletRecipe = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_TABLET_RECIPE);
            if (tabletRecipe == null) {
                tabletRecipe = new TabletRecipeFragment();
            }
        } else {
            stepList = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_STEP_LIST);
            if (stepList == null) {
                stepList = new StepListFragment();
            }
            phoneStepPager = getSupportFragmentManager()
                    .findFragmentByTag(TAG_FRAGMENT_PHONE_STEP_PAGER);
            if (phoneStepPager == null) {
                phoneStepPager = new ViewPagerFragment();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null && intent.hasExtra(KEY_INTENT_EXTRA_RECIPE)) {
            Recipe recipe = intent.getParcelableExtra(KEY_INTENT_EXTRA_RECIPE);
            EventBus.getDefault().post(new RecipeHolder.RecipeCardClickEvent(recipe));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onChangeAppBarTitleEvent(ChangeAppBarTitleEvent event) {
        if (event.title == null) return;
        setTitle(event.title);
    }

    @Subscribe
    public void onRecipeCardClick(RecipeHolder.RecipeCardClickEvent event) {
        if (event.recipe == null) return;
        Bundle args = new Bundle();
        if (isTablet) {
            args.putParcelable(TabletRecipeFragment.KEY_ARGUMENT_RECIPE, event.recipe);
            tabletRecipe.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .addToBackStack(TAG_FRAGMENT_TABLET_RECIPE)
                    .replace(R.id.fl_main_fragment_holder, tabletRecipe, TAG_FRAGMENT_TABLET_RECIPE)
                    .commit();
        } else {
            args.putParcelable(StepListFragment.KEY_ARGUMENT_RECIPE, event.recipe);
            stepList.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .addToBackStack(TAG_FRAGMENT_STEP_LIST)
                    .replace(R.id.fl_main_fragment_holder, stepList, TAG_FRAGMENT_STEP_LIST)
                    .commit();
        }
        setTitle(event.recipe.getName());

        updateIngredientsWidget(event.recipe);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void updateIngredientsWidget(Recipe recipe) {
        Intent clickedRecipe = new Intent(this, IngredientsWidget.class);
        clickedRecipe.setAction(IngredientsWidget.ACTION_UPDATE_INGREDIENTS);
        clickedRecipe.putExtra(IngredientsWidget.KEY_INTENT_EXTRA_RECIPE,
                recipe);
        sendBroadcast(clickedRecipe);
    }

    // Used only on phones
    @Subscribe
    public void onStepClickEvent(StepHolder.StepClickEvent event) {
        if (event.recipe == null || isTablet) {
            return;
        }
        Bundle args = new Bundle();
        args.putParcelable(ViewPagerFragment.KEY_ARGUMENTS_RECIPE, event.recipe);
        args.putInt(ViewPagerFragment.KEY_ARGUMENTS_CLICKED_POSITION, event.clickedPosition);
        phoneStepPager = new ViewPagerFragment();
        phoneStepPager.setArguments(args);
        getSupportFragmentManager().beginTransaction()
                .addToBackStack(TAG_FRAGMENT_PHONE_STEP_PAGER)
                .replace(R.id.fl_main_fragment_holder, phoneStepPager, TAG_FRAGMENT_PHONE_STEP_PAGER)
                .commit();
    }

    @VisibleForTesting
    public IdlingResource getIdlingResource() {
        CustomIdlingResource.setupIdlingResource();
        if (CustomIdlingResource.getInstance() != null) {
            CustomIdlingResource.getInstance().setIdleState(false);
        }
        return CustomIdlingResource.getInstance();
    }

    public static class ChangeAppBarTitleEvent {

        public final String title;

        public ChangeAppBarTitleEvent(String title) {
            this.title = title;
        }
    }
}