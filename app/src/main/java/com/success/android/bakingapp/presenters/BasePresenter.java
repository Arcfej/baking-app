package com.success.android.bakingapp.presenters;

import android.os.Bundle;

import com.success.android.bakingapp.views.BaseView;

public interface BasePresenter {

    /**
     * Must call in onStart(), when the UI is already visible. Request data in this method.
     * @param view
     */
    void attachView(BaseView view);

    Bundle getState();
}
