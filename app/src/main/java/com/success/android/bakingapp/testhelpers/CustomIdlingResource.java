package com.success.android.bakingapp.testhelpers;

import android.support.annotation.Nullable;
import android.support.test.espresso.IdlingResource;

public class CustomIdlingResource implements IdlingResource {

    @Nullable
    private static CustomIdlingResource instance = null;

    public static synchronized void setupIdlingResource() {
        if (instance == null) {
            instance = new CustomIdlingResource();
        }
    }

    @Nullable
    public static synchronized CustomIdlingResource getInstance() {
        return instance;
    }

    private CustomIdlingResource() {
        isIdle = true;
    }

    private ResourceCallback resourceCallback;
    private boolean isIdle;

    @Override
    public String getName() {
        return this.getClass().getName();
    }

    @Override
    public boolean isIdleNow() {
        return isIdle;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback callback) {
        this.resourceCallback = callback;
    }

    public void setIdleState(boolean isIdle) {
        this.isIdle = isIdle;
        if (isIdle && resourceCallback != null) {
            resourceCallback.onTransitionToIdle();
        }
    }
}
