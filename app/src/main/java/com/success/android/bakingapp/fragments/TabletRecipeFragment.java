package com.success.android.bakingapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.success.android.bakingapp.R;
import com.success.android.bakingapp.pojos.Recipe;
import com.success.android.bakingapp.views.steplist.StepHolder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class TabletRecipeFragment extends android.support.v4.app.Fragment {

    private static final String TAG_FRAGMENT_STEP_LIST = "steps";
    private static final String TAG_FRAGMENT_STEP_DETAILS = "details";

    public static final String KEY_ARGUMENT_RECIPE = "recipe";

    private static final String KEY_STATE_RECIPE = "recipe";

    // Need to save with state
    private Bundle presenterState = new Bundle();
    private Recipe recipe;

    private Fragment stepList;
    private Fragment stepDetails;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_STATE_RECIPE)) {
            recipe = savedInstanceState.getParcelable(KEY_STATE_RECIPE);
        } else if (getArguments() == null || !getArguments().containsKey(KEY_ARGUMENT_RECIPE)) {
            throw new IllegalArgumentException("This fragment need a Recipe (in the arguments");
        } else {
            recipe = getArguments().getParcelable(KEY_ARGUMENT_RECIPE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(
                R.layout.fragment_tablet_master_details,
                container,
                false
        );
    }

    @Override
    public void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);

        stepList = getChildFragmentManager().findFragmentByTag(TAG_FRAGMENT_STEP_LIST);
        if (stepList == null) {
            stepList = new StepListFragment();
            Bundle listArgs = new Bundle();
            listArgs.putParcelable(StepListFragment.KEY_ARGUMENT_RECIPE, recipe);
            stepList.setArguments(listArgs);
            getChildFragmentManager().beginTransaction()
                    .add(R.id.fl_tablet_left_list, stepList, TAG_FRAGMENT_STEP_LIST)
                    .commit();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(KEY_STATE_RECIPE, recipe);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onStepClickEvent(StepHolder.StepClickEvent event) {
        stepDetails = new StepDetailsFragment();

        Bundle detailsArgs = new Bundle();
        detailsArgs.putParcelable(StepDetailsFragment.KEY_ARGUMENTS_RECIPE, event.recipe);
        detailsArgs.putInt(StepDetailsFragment.KEY_ARGUMENTS_STEP_INDEX, event.clickedPosition);
        stepDetails.setArguments(detailsArgs);

        getChildFragmentManager().beginTransaction()
                .replace(R.id.fl_tablet_right_details, stepDetails, TAG_FRAGMENT_STEP_DETAILS)
                .commit();
    }
}