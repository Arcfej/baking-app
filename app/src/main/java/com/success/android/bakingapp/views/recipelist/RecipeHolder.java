package com.success.android.bakingapp.views.recipelist;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.success.android.bakingapp.R;
import com.success.android.bakingapp.pojos.Recipe;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeHolder extends RecyclerView.ViewHolder implements RecipeListItem {

    @BindView(R.id.tv_recipe_card_name)
    TextView recipeNameView;
    @BindView(R.id.tv_recipe_card_servings)
    TextView servingsView;

    private RecipeHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public static RecipeHolder constructWithInflating(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_recipe_card, parent, false);
        return new RecipeHolder(itemView);
    }

    @Override
    public void setRecipeName(String name) {
        recipeNameView.setText(name);
    }

    @Override
    public void setRecipeServings(int count) {
        String text = itemView.getContext().getResources().getString(R.string.recipe_card_servings, count);
        servingsView.setText(text);
    }

    @Override
    public void setRecipeBackground(String uri) {
        Glide.with(itemView).clear(itemView);
        Glide.with(itemView).load(uri).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource,
                                        @Nullable Transition<? super Drawable> transition) {
                itemView.setBackground(resource);
            }
        });
    }

    @Override
    public void setOnRecipeClickListener(final Recipe recipe) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new RecipeCardClickEvent(recipe));
            }
        });
    }

    public static class RecipeCardClickEvent {

        public final Recipe recipe;

        public RecipeCardClickEvent(Recipe recipe) {
            this.recipe = recipe;
        }
    }
}
