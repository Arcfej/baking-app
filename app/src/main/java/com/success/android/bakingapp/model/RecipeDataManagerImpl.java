package com.success.android.bakingapp.model;

import android.net.Uri;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.myapp.MyEventBusIndex;
import com.success.android.bakingapp.model.apirecipe.ApiRecipeContract;
import com.success.android.bakingapp.model.network.NetworkHelper;
import com.success.android.bakingapp.pojos.Recipe;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;

import java.util.List;

public class RecipeDataManagerImpl implements RecipeDataManager, JSONArrayRequestListener {

    // Singleton object
    private static RecipeDataManagerImpl INSTANCE;

    // EventBus just for the model layer
    private final EventBus modelEventBus;

    private RecipeDataManagerImpl() {
        modelEventBus = EventBus.builder().addIndex(new MyEventBusIndex()).build();
    }

    public static synchronized RecipeDataManagerImpl getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RecipeDataManagerImpl();
        }
        return INSTANCE;
    }

    private List<Recipe> recipeList;

    @Override
    public void requestRecipeList() {
        if (recipeList == null || recipeList.size() == 0) {
            NetworkHelper.startDownloadJSONArray(
                    Uri.parse(ApiRecipeContract.URL_RECIPE_LIST),
                    null,
                    null,
                    this
            );
        } else {
            EventBus.getDefault().post(new RecipeListReadyEvent(recipeList));
        }
    }

    @Override
    public void onResponse(JSONArray response) {
        recipeList = JsonHelper.parseRecipeListFromJsonArray(response);
        EventBus.getDefault().post(new RecipeListReadyEvent(recipeList));
    }

    @Override
    public void onError(ANError anError) {
        // TODO implement
        String h = "g";
    }

    public static class RecipeListReadyEvent {

        public final List<Recipe> recipeList;

        RecipeListReadyEvent(List<Recipe> recipeList) {
            this.recipeList = recipeList;
        }
    }
}