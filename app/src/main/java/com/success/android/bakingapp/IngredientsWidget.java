package com.success.android.bakingapp;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;

import com.success.android.bakingapp.pojos.Recipe;

/**
 * Implementation of App Widget functionality.
 */
public class IngredientsWidget extends AppWidgetProvider {

    public static final String ACTION_UPDATE_INGREDIENTS =
            "com.success.android.bakingapp.IngredientsWidget.UPDATE_INGREDIENTS";

    public static final String KEY_INTENT_EXTRA_RECIPE = "recipe";

    private static Recipe recipe;

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.ingredients_widget);

        Intent intent = new Intent(context, MainActivity.class);
        if (recipe != null) {
            views.setTextViewText(R.id.tv_ingredients_widget_text, recipe.getIngredientsAsText());
            intent.putExtra(MainActivity.KEY_INTENT_EXTRA_RECIPE, recipe);
        } else {
            views.setTextViewText(R.id.tv_ingredients_widget_text,
                    context.getString(R.string.ingredients_widget_default_text));
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                MainActivity.REQUEST_CODE_OPEN_FROM_INGREDIENTS_WIDGET,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        views.setOnClickPendingIntent(R.id.tv_ingredients_widget_text, pendingIntent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    // TODO home screen not recognise Recipe class?
    @SuppressLint("ApplySharedPref")
    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent != null &&
                intent.getAction() != null &&
                intent.getAction().equals(ACTION_UPDATE_INGREDIENTS) &&
                intent.hasExtra(KEY_INTENT_EXTRA_RECIPE)) {

            Bundle extras = intent.getExtras();
            recipe = extras.getParcelable(KEY_INTENT_EXTRA_RECIPE);
            AppWidgetManager widgetManager = AppWidgetManager.getInstance(context);
            ComponentName component = new ComponentName(context, this.getClass());
            onUpdate(context, widgetManager, widgetManager.getAppWidgetIds(component));
        }
    }
}