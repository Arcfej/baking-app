package com.success.android.bakingapp.presenters;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.success.android.bakingapp.BakingApplication;
import com.success.android.bakingapp.model.RecipeDataManager;
import com.success.android.bakingapp.pojos.Recipe;
import com.success.android.bakingapp.views.BaseView;
import com.success.android.bakingapp.views.stepdetails.StepDetailsView;

public class StepDetailsPresenterImpl implements StepDetailsPresenter {

    private static final String KEY_VIDEO_PLAYER_POSITION = "video_position";

    private Context appContext;

    private RecipeDataManager dataManager;
    private ExoPlayer videoPlayer;
    private long savedVideoPosition;

    private boolean isPlayerPrepared = false;

    @NonNull
    private Recipe recipe;
    private int stepIndex;
    // Null, if this details is about the ingredients
    @Nullable
    private Recipe.Step currentStep = null;

    @Nullable
    private StepDetailsView view;

    public StepDetailsPresenterImpl(Context context,
                                    RecipeDataManager dataManager,
                                    @NonNull Recipe recipe,
                                    int stepIndex,
                                    Bundle presenterState) {
        this.appContext = context.getApplicationContext();
        this.dataManager = dataManager;
        videoPlayer = ExoPlayerFactory.newSimpleInstance(
                appContext,
                new DefaultTrackSelector(new DefaultBandwidthMeter())
        );
        this.recipe = recipe;
        this.stepIndex = stepIndex;
        if (stepIndex != 0) {
            currentStep = recipe.getSteps().get(stepIndex - 1);
        }
        if (presenterState != null && presenterState.containsKey(KEY_VIDEO_PLAYER_POSITION)) {
            savedVideoPosition = presenterState.getLong(KEY_VIDEO_PLAYER_POSITION);
        }
    }

    @Override
    public void attachView(BaseView view) {
        if (view == null) {
            return;
        }
        this.view = (StepDetailsView) view;
        if (currentStep == null) {
            this.view.setInstructions(recipe.getIngredientsAsText());
        } else {
            this.view.setInstructions(currentStep.getDescription());
        }
    }



    @Override
    public Bundle getState() {
        Bundle state = new Bundle();
        state.putLong(KEY_VIDEO_PLAYER_POSITION, savedVideoPosition);
        return state;
    }

    @Override
    public void preparePlayer() {
        if (currentStep != null && !TextUtils.isEmpty(currentStep.getVideoUrl())) {
            videoPlayer.release();
            videoPlayer = ExoPlayerFactory.newSimpleInstance(
                    appContext,
                    new DefaultTrackSelector(new DefaultBandwidthMeter())
            );
            DefaultDataSourceFactory dataSource = new DefaultDataSourceFactory(
                    appContext,
                    Util.getUserAgent(appContext, BakingApplication.getApplicationName(appContext))
            );
            MediaSource videoSource = new ExtractorMediaSource.Factory(dataSource)
                    .createMediaSource(Uri.parse(currentStep.getVideoUrl()));
            videoPlayer.seekTo(savedVideoPosition);
            videoPlayer.prepare(videoSource, false, false);
            videoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
            if (this.view != null) {
                this.view.setVideoPlayer(videoPlayer);
                isPlayerPrepared = true;
            }
        }
    }

    @Override
    public void startPlayingVideo() {
        if (isPlayerPrepared) {
            videoPlayer.setPlayWhenReady(true);
        }
    }

    @Override
    public void resetPlayer() {
        savedVideoPosition = videoPlayer.getCurrentPosition();
        videoPlayer.stop(true);
    }

    @Override
    public void releasePlayer() {
        videoPlayer.release();
        isPlayerPrepared = false;
    }
}