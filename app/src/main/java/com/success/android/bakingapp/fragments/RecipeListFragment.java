package com.success.android.bakingapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.success.android.bakingapp.MainActivity;
import com.success.android.bakingapp.R;
import com.success.android.bakingapp.model.RecipeDataManagerImpl;
import com.success.android.bakingapp.presenters.RecipeCardAdapter;
import com.success.android.bakingapp.views.recipelist.RecipeListView;
import com.success.android.bakingapp.views.recipelist.RecipeListViewImpl;

import org.greenrobot.eventbus.EventBus;

public class RecipeListFragment extends Fragment {

    private RecipeCardAdapter presenter;
    private RecipeListView view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        presenter = new RecipeCardAdapter(RecipeDataManagerImpl.getInstance());

        view = new RecipeListViewImpl(
                inflater,
                container,
                presenter,
                getResources().getBoolean(R.bool.isTablet),
                getResources().getBoolean(R.bool.isPortrait)
        );

        return view.getRootView();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(presenter);
        presenter.attachView(view);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new MainActivity.ChangeAppBarTitleEvent(
                getString(R.string.app_name)));
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(presenter);
    }
}