package com.success.android.bakingapp.presenters;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.success.android.bakingapp.R;
import com.success.android.bakingapp.fragments.StepDetailsFragment;
import com.success.android.bakingapp.pojos.Recipe;
import com.success.android.bakingapp.views.BaseView;

public class StepPagerAdapter extends FragmentStatePagerAdapter implements BasePresenter {

    private static final String KEY_PAGER_ADAPTER_STATE = "adapter_state";

    private Resources resources;

    private Recipe recipe;

    public StepPagerAdapter(FragmentManager fm, Resources resources, Recipe recipe,
                            Bundle presenterState) {
        super(fm);
        this.resources = resources;
        this.recipe = recipe;
        if (presenterState != null) {
            restoreState(
                    presenterState.getParcelable(KEY_PAGER_ADAPTER_STATE),
                    this.getClass().getClassLoader()
            );
        }
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new StepDetailsFragment();
        Bundle args = new Bundle();
        args.putInt(StepDetailsFragment.KEY_ARGUMENTS_STEP_INDEX, position);
        args.putParcelable(StepDetailsFragment.KEY_ARGUMENTS_RECIPE, recipe);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return resources.getString(R.string.step_list_ingredients_title);
        } else {
            return recipe.getSteps().get(position - 1).getShortDescription();
        }
    }

    @Override
    public int getCount() {
        if (recipe == null) return 0;
        else return recipe.getSteps().size() + 1;
    }

    @Override
    public void attachView(BaseView view) {

    }

    @Override
    public Bundle getState() {
        Bundle state = new Bundle();
        state.putParcelable(KEY_PAGER_ADAPTER_STATE, saveState());
        return state;
    }
}