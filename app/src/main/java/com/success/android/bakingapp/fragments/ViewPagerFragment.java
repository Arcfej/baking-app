package com.success.android.bakingapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.success.android.bakingapp.pojos.Recipe;
import com.success.android.bakingapp.presenters.StepPagerAdapter;
import com.success.android.bakingapp.views.phonesteppager.StepPagerView;
import com.success.android.bakingapp.views.phonesteppager.StepPagerViewImpl;

public class ViewPagerFragment extends android.support.v4.app.Fragment {

    public static final String KEY_ARGUMENTS_RECIPE = "recipe";
    public static final String KEY_ARGUMENTS_CLICKED_POSITION = "position";

    private static final String KEY_PRESENTER_STATE = "presenter_state";
    private static final String KEY_RECIPE_STATE = "recipe";

    // Need to save with state
    private Bundle presenterState = new Bundle();
    private Recipe recipe;

    // Need to recreate
    private StepPagerView view;
    private StepPagerAdapter presenter;
    private int clickedPosition;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_RECIPE_STATE)) {
            presenterState = savedInstanceState.getBundle(KEY_PRESENTER_STATE);
            recipe = savedInstanceState.getParcelable(KEY_RECIPE_STATE);
        }
        if (getArguments() == null || !getArguments().containsKey(KEY_ARGUMENTS_RECIPE)) {
            throw new IllegalArgumentException("This fragment need a Recipe (in the arguments");
        } else {
            recipe = getArguments().getParcelable(KEY_ARGUMENTS_RECIPE);
            clickedPosition = getArguments().getInt(KEY_ARGUMENTS_CLICKED_POSITION);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        presenter = new StepPagerAdapter(getChildFragmentManager(),
                getResources(), recipe, presenterState);
        view = new StepPagerViewImpl(inflater, container, presenter, clickedPosition);
        return view.getRootView();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.attachView(view);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(KEY_RECIPE_STATE, recipe);
        outState.putBundle(KEY_PRESENTER_STATE, presenter.getState());
    }

    @Override
    public void onPause() {
        super.onPause();
        presenterState = presenter.getState();
    }
}