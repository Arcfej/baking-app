package com.success.android.bakingapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.success.android.bakingapp.R;
import com.success.android.bakingapp.pojos.Recipe;
import com.success.android.bakingapp.presenters.RecipeStepAdapter;
import com.success.android.bakingapp.views.steplist.StepListView;
import com.success.android.bakingapp.views.steplist.StepListViewImpl;

import org.greenrobot.eventbus.EventBus;

public class StepListFragment extends android.support.v4.app.Fragment {

    public static final String KEY_ARGUMENT_RECIPE = "recipe";
    private static final String KEY_STATE_RECIPE = "recipe";
    private static final String KEY_STATE_PRESENTER = "presenter_state";

    // Need to save with state
    private Recipe recipe;
    private Bundle presenterState;

    // Need to recreate
    private StepListView view;
    private RecipeStepAdapter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_STATE_RECIPE)) {
                recipe = savedInstanceState.getParcelable(KEY_STATE_RECIPE);
            }
            if (savedInstanceState.containsKey(KEY_STATE_PRESENTER)) {
                presenterState = savedInstanceState.getBundle(KEY_STATE_PRESENTER);
            }
        } else if (getArguments() == null || !getArguments().containsKey(KEY_ARGUMENT_RECIPE)) {
            throw new IllegalArgumentException("You have to set a Recipe in arguments to this fragment");
        } else {
            recipe = getArguments().getParcelable(KEY_ARGUMENT_RECIPE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        presenter = new RecipeStepAdapter(recipe, getResources(), presenterState);

        view = new StepListViewImpl(inflater, container, presenter);
        return view.getRootView();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getResources().getBoolean(R.bool.isTablet)) {
            EventBus.getDefault().register(presenter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.attachView(view);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_STATE_RECIPE, recipe);
        outState.putBundle(KEY_STATE_PRESENTER, presenter.getState());
    }

    @Override
    public void onPause() {
        super.onPause();
        presenterState = presenter.getState();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (getResources().getBoolean(R.bool.isTablet)) {
            EventBus.getDefault().unregister(presenter);
        }
    }
}