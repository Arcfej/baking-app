package com.success.android.bakingapp.model;

public interface RecipeDataManager {

    void requestRecipeList();
}
